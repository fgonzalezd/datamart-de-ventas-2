package definitions;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.vimalselvam.cucumber.listener.Reporter;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.EnvironmentParcom_Page;
import pages.RubrosInsert_Page;
import properties.PropertiesInit;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import static org.junit.Assert.assertEquals;

public class ParcatSectorial_N8_Def {

    public static PropertiesInit prop;
    public static EnvironmentParcom_Page environment;
    public static RubrosInsert_Page page;
    public static String insertXML = "src/test/resources/insert/Insert_ParcatSectorial.xml";
    private String[] origenCS = new String[2];
    private String[] destinoCS = new String[2];

    @Given("^se debe limpiar el ambiente para la ejecucion de pruebas para parcat_Sectorial caso N_Ocho$")
    public void se_debe_limpiar_el_ambiente_para_la_ejecucion_de_pruebas_para_parcat_Sectorial_caso_N_Ocho() throws Throwable {
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.SshUser(), prop.SshPass(), "hdfs dfs -rm -r /data/environment/hdp_lake/stg_parcom/par_cat_sectorial/TO_TRANSFER/*");
        page.sshConnector(prop.SshHostSlave(), prop.SshPortSlave(), prop.SshUserSlave(), prop.SshPassSlave(), "hdfs dfs -rm -r /data/environment/hdp_lake/stg_parcom/par_cat_sectorial/TO_IMPORT/*");
        page.jdbcConnector(prop.OracleDriver(), prop.OracleURL(), prop.OracleUser(), prop.OraclePass(), prop.OracleScheme(), environment.dropPAR_CAT_SECTORIAL());
        page.jdbcConnector(prop.OracleDriver(), prop.OracleURL(), prop.OracleUser(), prop.OraclePass(), prop.OracleScheme(), environment.createPAR_CAT_SECTORIAL());
    }

    @When("^se debe insertar el dataset para las tablas de Oracle para parcat_Sectorial caso N_Ocho$")
    public void se_debe_insertar_el_dataset_para_las_tablas_de_Oracle_para_parcat_Sectorial_caso_N_Ocho() throws Throwable {
        page.jdbcConnector(prop.OracleDriver(), prop.OracleURL(), prop.OracleUser(), prop.OraclePass(), prop.OracleScheme(), page.insert_ORIGEN(insertXML));
    }

    @Then("^se debe ejecutar el proceso de extraccion para parcat_Sectorial caso N_Ocho$")
    public void se_debe_ejecutar_el_proceso_de_extraccion_para_parcat_Sectorial_caso_N_Ocho() throws Throwable {
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.SshUser(), prop.SshPass(),
                "sh -x /data/environment/hdp_app/load_raw/generic_bash/shells_genericas/shl/Shl_extrae_sqoop.sh 21 20190710 PARCOM N");

        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.SshUser(), prop.SshPass(),
                "sh -x /data/environment/hdp_app/load_raw/generic_bash/shells_genericas/shl/Shl_extrae_sqoop.sh 21 20190711 PARCOM N");

        page.sshConnector(prop.SshHostSlave(), prop.SshPortSlave(), prop.SshUserSlave(), prop.SshPassSlave(),
                "sh -x /data/environment/hdp_app/load_raw/generic_bash/shells_genericas/shl/Shl_transfiere_archivo.sh 23 20190710 PARCOM N");

        page.sshConnector(prop.SshHostSlave(), prop.SshPortSlave(), prop.SshUserSlave(), prop.SshPassSlave(),
                "sh -x /data/environment/hdp_app/load_raw/generic_bash/shells_genericas/shl/Shl_transfiere_archivo.sh 23 20190711 PARCOM N");
    }

    @When("^hago una consulta al checksum de los archivos de origen parcat_Sectorial caso N_Ocho$")
    public void hago_una_consulta_al_checksum_de_los_archivos_de_origen_parcat_Sectorial_caso_N_Ocho() throws Throwable {
        JSch jschSSH = new JSch();
        Session sessionSSH = null;
        ChannelExec channelSSH = null;

        try {
            sessionSSH = jschSSH.getSession(prop.SshUser(), prop.SshHost(), prop.SshPort());
            sessionSSH.setPassword(prop.SshPass());

            // evita preguntar por confirmacion al conectarse
            sessionSSH.setConfig("StrictHostKeyChecking", "no");
            sessionSSH.connect();

            // inicia canal SSH
            channelSSH = (ChannelExec) sessionSSH.openChannel("exec");

            // executar comando
            channelSSH.setCommand("hdfs dfs -checksum /data/environment/hdp_lake/stg_parcom/par_cat_sectorial/TO_TRANSFER/");
            channelSSH.connect();

            // muestra log de la ejecucion
            InputStream in = channelSSH.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            for (int i = 0; i < 2; i++) {
                origenCS[i] = reader.readLine();
                System.out.println(origenCS[i]);
            }

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (sessionSSH != null) {
                sessionSSH.disconnect();
            }
            if (channelSSH != null) {
                channelSSH.isClosed();
            }
        }
    }

    @When("^hago una consulta al checksum de los archivos transferidos parcat_Sectorial caso N_Ocho$")
    public void hago_una_consulta_al_checksum_de_los_archivos_transferidos_parcat_Sectorial_caso_N_Ocho() throws Throwable {
        JSch jschSSH = new JSch();
        Session sessionSSH = null;
        ChannelExec channelSSH = null;

        try {
            sessionSSH = jschSSH.getSession(prop.SshUserSlave(), prop.SshHostSlave(), prop.SshPortSlave());
            sessionSSH.setPassword(prop.SshPassSlave());

            // evita preguntar por confirmacion al conectarse
            sessionSSH.setConfig("StrictHostKeyChecking", "no");
            sessionSSH.connect();

            // inicia canal SSH
            channelSSH = (ChannelExec) sessionSSH.openChannel("exec");

            // executar comando
            channelSSH.setCommand("hdfs dfs -checksum /data/environment/hdp_lake/stg_parcom/par_cat_sectorial/TO_IMPORT/");
            channelSSH.connect();

            // muestra log de la ejecucion
            InputStream in = channelSSH.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            for (int i = 0; i < 2; i++) {
                destinoCS[i] = reader.readLine();
                System.out.println(destinoCS[i]);
            }

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (sessionSSH != null) {
                sessionSSH.disconnect();
            }
            if (channelSSH != null) {
                channelSSH.isClosed();
            }
        }
    }

    @Then("^valido que los archivos transferidos esten correcto parcat_Sectorial caso N_Ocho$")
    public void valido_que_los_archivos_transferidos_esten_correcto_parcat_Sectorial_caso_N_Ocho() throws Throwable {
        for (int i = 0; i < 2; i++) {
            Reporter.addStepLog("Checksum Master= " + origenCS[i]);
            Reporter.addStepLog("Checksum Slave = " + destinoCS[i]);
            Reporter.addStepLog("------------------------------------------------------------------------------");
            assertEquals(origenCS[i], destinoCS[i]);
        }
    }

    @Then("^se debe limpiar el ambiente luego de la ejecucion de pruebas para parcat_Sectorial caso N_Ocho$")
    public void se_debe_limpiar_el_ambiente_luego_de_la_ejecucion_de_pruebas_para_parcat_Sectorial_caso_N_Ocho() throws Throwable {
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.SshUser(), prop.SshPass(), "hdfs dfs -rm -r /data/environment/hdp_lake/stg_parcom/par_cat_sectorial/TO_TRANSFER/*");
        page.sshConnector(prop.SshHostSlave(), prop.SshPortSlave(), prop.SshUserSlave(), prop.SshPassSlave(), "hdfs dfs -rm -r /data/environment/hdp_lake/stg_parcom/par_cat_sectorial/TO_IMPORT/*");
        page.jdbcConnector(prop.OracleDriver(), prop.OracleURL(), prop.OracleUser(), prop.OraclePass(), prop.OracleScheme(), environment.dropPAR_CAT_SECTORIAL());
    }
}
