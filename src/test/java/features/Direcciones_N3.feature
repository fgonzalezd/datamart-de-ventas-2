@direcciones_N3

Feature: Direcciones N3 - La fecha de los datos ya fue ejecutada con ERROR

  Scenario: Ambientacion para Direcciones "La fecha de los datos ya fue ejecutada con ERROR"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para Direcciones caso N_tres
    When se debe insertar el dataset para las tablas de Oracle para Direcciones caso N_tres
    And se debe insertar el dataset para las tablas de hive para Direcciones caso N_tres
    Then se debe ejecutar el proceso de extraccion para Direcciones caso N_tres

  Scenario: Validacion para tabla Direcciones "La fecha de los datos ya fue ejecutada con ERROR"
    When hago una consulta a la tabla del RAW para tabla Direcciones caso N_tres
    And hago una consulta a la tabla del MCP para tabla Direcciones caso N_tres
    Then valido que la insercion a la tabla RAW sea correcta para tabla Direcciones caso N_tres
    And valido que el registro de insercion en el MCP fue correcto para tabla Direcciones caso N_tres

  Scenario: Validacion de archivos en hdfs para Direcciones "La fecha de los datos ya fue ejecutada con ERROR"
    When hago una consulta a los archivos en TO_IMPORT para Direcciones caso N_tres
    And hago una consulta a los archivos en TO_TRANSFER para Direcciones caso N_tres
    And hago una consulta a los archivos en LOADING para Direcciones caso N_tres
    Then valido que la cantidad de archivos en el hdfs sea correcta para Direcciones caso N_tres

  Scenario: Validacion de logs Direcciones "La fecha de los datos ya fue ejecutada con ERROR"
    When hago una consulta a los logs generados en RAW para Direcciones caso N_tres
    Then valido que la cantidad de logs generados sea correcto para Direcciones caso N_tres

  Scenario: Limpieza ambientacion para Direcciones "La fecha de los datos ya fue ejecutada con ERROR"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Direcciones caso N_tres