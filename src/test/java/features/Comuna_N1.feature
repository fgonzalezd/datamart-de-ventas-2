@comuna_N1

Feature: Comuna N1 - La fecha de los datos no ha sido ejecutada

  Scenario: Ambientacion para Comuna "La fecha de los datos no ha sido ejecutada"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para Comuna caso N_uno
    When se debe insertar el dataset para las tablas de Oracle para Comuna caso N_uno
    And se debe insertar el dataset para las tablas de hive para Comuna caso N_uno
    Then se debe ejecutar el proceso de extraccion para Comuna caso N_uno

  Scenario: Validacion para tabla Comuna "La fecha de los datos no ha sido ejecutada"
    When hago una consulta a la tabla del RAW para tabla Comuna caso N_uno
    And hago una consulta a la tabla del MCP para tabla Comuna caso N_uno
    Then valido que la insercion a la tabla RAW sea correcta para tabla Comuna caso N_uno
    And valido que el registro de insercion en el MCP fue correcto para tabla Comuna caso N_uno

  Scenario: Validacion de archivos en hdfs para Comuna "La fecha de los datos no ha sido ejecutada"
    When hago una consulta a los archivos en TO_IMPORT para Comuna caso N_uno
    And hago una consulta a los archivos en TO_TRANSFER para Comuna caso N_uno
    And hago una consulta a los archivos en LOADING para Comuna caso N_uno
    Then valido que la cantidad de archivos en el hdfs sea correcta para Comuna caso N_uno

  Scenario: Validacion de logs Comuna "La fecha de los datos no ha sido ejecutada"
    When hago una consulta a los logs generados en RAW para Comuna caso N_uno
    Then valido que la cantidad de logs generados sea correcto para Comuna caso N_uno

  Scenario: Limpieza ambientacion para Comuna "La fecha de los datos no ha sido ejecutada"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Comuna caso N_uno