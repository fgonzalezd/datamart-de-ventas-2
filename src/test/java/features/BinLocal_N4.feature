@binLocal_N4

Feature: Bin_Local N4 - Metrica deshabilitada

  Scenario: Ambientacion para Bin_Local "Metrica deshabilitada"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para Bin_Local caso N_cuatro
    When se debe insertar el dataset para las tablas de Oracle para Bin_Local caso N_cuatro
    And se debe insertar el dataset para las tablas de hive para Bin_Local caso N_cuatro
    Then se debe ejecutar el proceso de extraccion para Bin_Local caso N_cuatro

  Scenario: Validacion para tabla Bin_Local "Metrica deshabilitada"
    When hago una consulta a la tabla del RAW para tabla Bin_Local caso N_cuatro
    And hago una consulta a la tabla del MCP para tabla Bin_Local caso N_cuatro
    Then valido que la insercion a la tabla RAW sea correcta para tabla Bin_Local caso N_cuatro
    And valido que el registro de insercion en el MCP fue correcto para tabla Bin_Local caso N_cuatro

  Scenario: Validacion de archivos en hdfs para Bin_Local "Metrica deshabilitada"
    When hago una consulta a los archivos en TO_IMPORT para Bin_Local caso N_cuatro
    And hago una consulta a los archivos en TO_TRANSFER para Bin_Local caso N_cuatro
    And hago una consulta a los archivos en LOADING para Bin_Local caso N_cuatro
    Then valido que la cantidad de archivos en el hdfs sea correcta para Bin_Local caso N_cuatro

  Scenario: Validacion de logs Bin_Local "Metrica deshabilitada"
    When hago una consulta a los logs generados en RAW para Bin_Local caso N_cuatro
    Then valido que la cantidad de logs generados sea correcto para Bin_Local caso N_cuatro

  Scenario: Limpieza ambientacion para Bin_Local "Metrica deshabilitada"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Bin_Local caso N_cuatro