@comuna_N2

Feature: Comuna N2 - La fecha de los datos ya fue ejecutada OK

  Scenario: Ambientacion para Comuna "La fecha de los datos ya fue ejecutada OK"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para Comuna caso N_dos
    When se debe insertar el dataset para las tablas de Oracle para Comuna caso N_dos
    And se debe insertar el dataset para las tablas de hive para Comuna caso N_dos
    Then se debe ejecutar el proceso de extraccion para Comuna caso N_dos

  Scenario: Validacion para tabla Comuna "La fecha de los datos ya fue ejecutada OK"
    When hago una consulta a la tabla del RAW para tabla Comuna caso N_dos
    And hago una consulta a la tabla del MCP para tabla Comuna caso N_dos
    Then valido que la insercion a la tabla RAW sea correcta para tabla Comuna caso N_dos
    And valido que el registro de insercion en el MCP fue correcto para tabla Comuna caso N_dos

  Scenario: Validacion de archivos en hdfs para Comuna "La fecha de los datos ya fue ejecutada OK"
    When hago una consulta a los archivos en TO_IMPORT para Comuna caso N_dos
    And hago una consulta a los archivos en TO_TRANSFER para Comuna caso N_dos
    And hago una consulta a los archivos en LOADING para Comuna caso N_dos
    Then valido que la cantidad de archivos en el hdfs sea correcta para Comuna caso N_dos

  Scenario: Validacion de logs Comuna "La fecha de los datos ya fue ejecutada OK"
    When hago una consulta a los logs generados en RAW para Comuna caso N_dos
    Then valido que la cantidad de logs generados sea correcto para Comuna caso N_dos

  Scenario: Limpieza ambientacion para Comuna "La fecha de los datos ya fue ejecutada OK"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Comuna caso N_dos