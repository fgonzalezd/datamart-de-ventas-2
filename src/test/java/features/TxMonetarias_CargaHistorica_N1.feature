@txMonetarias_CargaHistorica_N1

Feature: tx_Monetarias Carga Historica - Cargar un rango de fecha de un mes específico

  Scenario: Ambientacion para tx_Monetarias Carga Historica "Cargar un rango de fecha de un mes específico"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para tx_Monetarias Carga Historica N_uno
    When transfiero el archivo a una carpeta del servidor para tx_Monetarias Carga Historica N_uno
    And muevo el archivo a una ruta hdfs para tx_Monetarias Carga Historica N_uno
    And consulto el checksum del archivo transferido para tx_Monetarias Carga Historica N_uno
    Then valido el checksum de ambos archivos para tx_Monetarias Carga Historica N_uno
    And se ejecuta el proceso para tx_Monetarias Carga Historica N_uno

  Scenario: Validacion para tabla tx_Monetarias Carga Historica "Cargar un rango de fecha de un mes específico"
    When hago una consulta a la tabla del RAW para tabla tx_Monetarias Carga Historica N_uno
    And hago una consulta a la tabla del MCP para tabla tx_Monetarias Carga Historica N_uno
    Then valido que la insercion a la tabla RAW sea correcta para tabla tx_Monetarias Carga Historica N_uno
    And valido que el registro de insercion en el MCP fue correcto para tabla tx_Monetarias Carga Historica N_uno

  Scenario: Limpieza ambientacion para tx_Monetarias Carga Historica "Cargar un rango de fecha de un mes específico"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para tx_Monetarias Carga Historica N_uno