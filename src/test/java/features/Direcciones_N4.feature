@direcciones_N4

Feature: Direcciones N4 - Metrica deshabilitada

  Scenario: Ambientacion para Direcciones "Metrica deshabilitada"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para Direcciones caso N_cuatro
    When se debe insertar el dataset para las tablas de Oracle para Direcciones caso N_cuatro
    And se debe insertar el dataset para las tablas de hive para Direcciones caso N_cuatro
    Then se debe ejecutar el proceso de extraccion para Direcciones caso N_cuatro

  Scenario: Validacion para tabla Direcciones "Metrica deshabilitada"
    When hago una consulta a la tabla del RAW para tabla Direcciones caso N_cuatro
    And hago una consulta a la tabla del MCP para tabla Direcciones caso N_cuatro
    Then valido que la insercion a la tabla RAW sea correcta para tabla Direcciones caso N_cuatro
    And valido que el registro de insercion en el MCP fue correcto para tabla Direcciones caso N_cuatro

  Scenario: Validacion de archivos en hdfs para Direcciones "Metrica deshabilitada"
    When hago una consulta a los archivos en TO_IMPORT para Direcciones caso N_cuatro
    And hago una consulta a los archivos en TO_TRANSFER para Direcciones caso N_cuatro
    And hago una consulta a los archivos en LOADING para Direcciones caso N_cuatro
    Then valido que la cantidad de archivos en el hdfs sea correcta para Direcciones caso N_cuatro

  Scenario: Validacion de logs Direcciones "Metrica deshabilitada"
    When hago una consulta a los logs generados en RAW para Direcciones caso N_cuatro
    Then valido que la cantidad de logs generados sea correcto para Direcciones caso N_cuatro

  Scenario: Limpieza ambientacion para Direcciones "Metrica deshabilitada"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Direcciones caso N_cuatro