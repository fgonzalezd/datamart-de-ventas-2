@contratos_N5

Feature: Contratos N5 - Metrica activa sin regla

  Scenario: Ambientacion para "Metrica activa sin regla"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para Contratos caso N_cinco
    When se debe insertar el dataset para las tablas de Oracle para Contratos caso N_cinco
    And se debe insertar el dataset para las tablas de hive para Contratos caso N_cinco
    Then se debe ejecutar el proceso de extraccion con una fecha del 9/7/2019 para Contratos caso N_cinco

  Scenario: Validacion para tabla Contratos "Metrica activa sin regla"
    When hago una consulta a la tabla del RAW para tabla Contratos caso N_cinco
    And hago una consulta a la tabla del MCP para tabla Contratos caso N_cinco
    Then valido que la insercion a la tabla RAW sea correcta para tabla Contratos caso N_cinco
    And valido que el registro de insercion en el MCP fue correcto para tabla Contratos caso N_cinco

  Scenario: Validacion de logs Contratos "Metrica activa sin regla"
    When hago una consulta a los logs generados en RAW para Contratos caso N_cinco
    Then valido que la cantidad de logs generados sea correcto para Contratos caso N_cinco

  Scenario: Limpieza ambientacion para "Metrica activa sin regla"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Contratos caso N_cinco