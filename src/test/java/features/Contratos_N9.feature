@contratos_N9

Feature: Contratos N9 - Ingreso de fecha erronea en el parametro

  Scenario: Ejecucion del proceso extraccion Contratos "Ingreso de fecha erronea en el parametro"
    When Se ejecuta el proceso de extraccion con una fecha erronea para Contratos caso N_Nueve
    Then se valida que el proceso de extraccion se haya terminado con error Contratos caso N_Nueve

  Scenario: Ejecucion del proceso STG Contratos "Ingreso de fecha erronea en el parametro"
    When Se ejecuta el proceso de carga STG con una fecha erronea para Contratos caso N_Nueve
    Then se valida que el proceso de carga STG se haya terminado con error Contratos caso N_Nueve

  Scenario: Ejecucion del proceso RAW Contratos "Ingreso de fecha erronea en el parametro"
    When Se ejecuta el proceso de carga RAW con una fecha erronea para Contratos caso N_Nueve
    Then se valida que el proceso de carga RAW se haya terminado con error Contratos caso N_Nueve

  Scenario: Validacion de logs Contratos "Ingreso de fecha erronea en el parametro"
    When hago una consulta a los logs generados en RAW para Contratos caso N_Nueve
    Then valido que la cantidad de logs generados sea correcto para Contratos caso N_Nueve