@eeccSpa_N7

Feature: EECC_SPA N7 - Transferencia de archivo unico DRP

  Scenario: Ambientacion para EECC_SPA "Transferencia de archivo unico DRP"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para EECC_SPA caso N_Siete
    When se debe insertar el dataset para las tablas de Oracle para EECC_SPA caso N_Siete
    Then se debe ejecutar el proceso de extraccion y transferencia para EECC_SPA caso N_Siete

  Scenario: Validacion de DRP EECC_SPA "Transferencia de archivo unico DRP"
    When hago una consulta al checksum del master EECC_SPA caso N_Siete
    And hago una consulta al checksum del slave EECC_SPA caso N_Siete
    Then valido que el archivo transferido este correcto EECC_SPA caso N_Siete

  Scenario: Validacion de archivos en hdfs para EECC_SPA "Transferencia de archivo unico DRP"
    When hago una consulta a los archivos en el hdfs del master para EECC_SPA caso N_Siete
    And hago una consulta a los archivos en el hdfs del slave para EECC_SPA caso N_Siete
    Then valido que la cantidad de archivos en el hdfs del master sea correcta para EECC_SPA caso N_Siete
    And valido que la cantidad de archivos en el hdfs del slave sea correcta para EECC_SPA caso N_Siete

  Scenario: Validacion de logs EECC_SPA "Transferencia de archivo unico DRP"
    When hago una consulta a los logs generados del master para EECC_SPA caso N_Siete
    And hago una consulta a los logs generados del slave para EECC_SPA caso N_Siete
    Then valido que la cantidad de logs generados del master sea correcto para EECC_SPA caso N_Siete
    And valido que la cantidad de logs generados del slave sea correcto para EECC_SPA caso N_Siete

  Scenario: Limpieza ambientacion para EECC_SPA "Transferencia de archivo unico DRP"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para EECC_SPA caso N_Siete