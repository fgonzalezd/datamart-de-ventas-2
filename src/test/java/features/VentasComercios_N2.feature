@ventasComercios_N2

Feature: Ventas_Comercios N2 - Datos incorrectos para la tabla vista

  Scenario: Ambientacion para "Datos incorrectos para la tabla vista"
    When se debe limpiar el ambiente para la ejecucion de pruebas de Ventas_Comercios caso N_dos
    Then se debe insertar el dataset para las tablas de CUR de Ventas_Comercios caso N_dos

  Scenario: Validacion para tabla Ventas_Comercios "Datos incorrectos para la tabla vista"
    When hago una consulta a la tabla LOB para vista Ventas_Comercios caso N_dos
    Then valido la visualizacion sea correcta para vista Ventas_Comercios caso N_dos

  Scenario: Limpieza ambientacion para "Datos incorrectos para la tabla vista"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Ventas_Comercios caso N_dos