@equipoxest_N7

Feature: Equipoxest N7 - Transferencia de archivo unico DRP

  Scenario: Ambientacion para Equipoxest "Transferencia de archivo unico DRP"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para Equipoxest caso N_Siete
    When se debe insertar el dataset para las tablas de Oracle para Equipoxest caso N_Siete
    Then se debe ejecutar el proceso de extraccion y transferencia para Equipoxest caso N_Siete

  Scenario: Validacion de DRP Equipoxest "Transferencia de archivo unico DRP"
    When hago una consulta al checksum del master Equipoxest caso N_Siete
    And hago una consulta al checksum del slave Equipoxest caso N_Siete
    Then valido que el archivo transferido este correcto Equipoxest caso N_Siete

  Scenario: Validacion de archivos en hdfs para Equipoxest "Transferencia de archivo unico DRP"
    When hago una consulta a los archivos en el hdfs del master para Equipoxest caso N_Siete
    And hago una consulta a los archivos en el hdfs del slave para Equipoxest caso N_Siete
    Then valido que la cantidad de archivos en el hdfs del master sea correcta para Equipoxest caso N_Siete
    And valido que la cantidad de archivos en el hdfs del slave sea correcta para Equipoxest caso N_Siete

  Scenario: Validacion de logs Equipoxest "Transferencia de archivo unico DRP"
    When hago una consulta a los logs generados del master para Equipoxest caso N_Siete
    And hago una consulta a los logs generados del slave para Equipoxest caso N_Siete
    Then valido que la cantidad de logs generados del master sea correcto para Equipoxest caso N_Siete
    And valido que la cantidad de logs generados del slave sea correcto para Equipoxest caso N_Siete

  Scenario: Limpieza ambientacion para Equipoxest "Transferencia de archivo unico DRP"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Equipoxest caso N_Siete