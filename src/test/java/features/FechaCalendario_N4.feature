@fechaCalendario_N4

Feature: Fecha_Calendario N4 - Metrica deshabilitada

  Scenario: Ambientacion para "Metrica deshabilitada"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para Fecha_Calendario caso N_cuatro
    When se debe insertar el dataset para las tablas de RAW para Fecha_Calendario caso N_cuatro
    And se debe insertar el dataset para las tablas de CUR para Fecha_Calendario caso N_cuatro
    Then se debe ejecutar el proceso de Fecha_Calendario caso N_cuatro

  Scenario: Validacion para tabla Fecha_Calendario "Metrica deshabilitada"
    When hago una consulta a la tabla del CUR para tabla Fecha_Calendario caso N_cuatro
    And hago una consulta a la tabla del MCP para tabla Fecha_Calendario caso N_cuatro
    Then valido que la insercion a la tabla CUR sea correcta para tabla Fecha_Calendario caso N_cuatro
    And valido que el registro de insercion en el MCP fue correcto para tabla Fecha_Calendario caso N_cuatro

  Scenario: Validacion de logs Fecha_Calendario "Metrica deshabilitada"
    When hago una consulta a los logs generados en RAW para Fecha_Calendario caso N_cuatro
    Then valido que la cantidad de logs generados sea correcto para Fecha_Calendario caso N_cuatro

  Scenario: Limpieza ambientacion para "Metrica deshabilitada"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Fecha_Calendario caso N_cuatro