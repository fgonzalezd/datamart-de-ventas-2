@contratos_N1

Feature: Contratos N1 - La fecha de los datos no ha sido ejecutada

  Scenario: Ambientacion para "La fecha de los datos no ha sido ejecutada"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para Contratos caso N_uno
    When se debe insertar el dataset para las tablas de Oracle para Contratos caso N_uno
    And se debe insertar el dataset para las tablas de hive para Contratos caso N_uno
    Then se debe ejecutar el proceso de extraccion con una fecha del 9/7/2019 para Contratos caso N_uno

  Scenario: Validacion para tabla Contratos "La fecha de los datos no ha sido ejecutada"
    When hago una consulta a la tabla del RAW para tabla Contratos caso N_uno
    And hago una consulta a la tabla del MCP para tabla Contratos caso N_uno
    Then valido que la insercion a la tabla RAW sea correcta para tabla Contratos caso N_uno
    And valido que el registro de insercion en el MCP fue correcto para tabla Contratos caso N_uno

  Scenario: Validacion de logs Contratos "La fecha de los datos no ha sido ejecutada"
    When hago una consulta a los logs generados en RAW para Contratos caso N_uno
    Then valido que la cantidad de logs generados sea correcto para Contratos caso N_uno

  Scenario: Limpieza ambientacion para "La fecha de los datos no ha sido ejecutada"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Contratos caso N_uno