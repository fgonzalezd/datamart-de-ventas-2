package runnings;

import cucumber.api.CucumberOptions;

@CucumberOptions(tags = "@actividadEconomica_N1,@actividadEconomica_N2,@actividadEconomica_N3,@actividadEconomica_N4,@actividadEconomica_N5,@actividadEconomica_N6,@actividadEconomica_N7,@actividadEconomica_N8")

public class ActividadEconomica_Running extends Running {
}
