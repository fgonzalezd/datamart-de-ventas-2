package pages;

public class QueryTableTxs_Page extends Base_Page {

    public static String queryXML = "src/test/resources/query/QueryTable_Txs.xml";

    /**********************
     ***** QUERY HIVE *****
     **********************/

    public static String queryTX_MONETARIAS() {
        String query = fetchQuery(queryXML).getProperty("queryTX_MONETARIAS");
        return query;
    }

    public static String queryTX_MONETARIAS_COUNT() {
        String query = fetchQuery(queryXML).getProperty("queryTX_MONETARIAS_COUNT");
        return query;
    }

    public static String queryTX_PROCESADAS() {
        String query = fetchQuery(queryXML).getProperty("queryTX_PROCESADAS");
        return query;
    }

    public static String queryTX_PROCESADAS_COUNT() {
        String query = fetchQuery(queryXML).getProperty("queryTX_PROCESADAS_COUNT");
        return query;
    }

    public static String queryVENTAS_COMERCIO() {
        String query = fetchQuery(queryXML).getProperty("queryVENTAS_COMERCIO");
        return query;
    }

    public static String queryVENTAS_COMERCIO_COUNT() {
        String query = fetchQuery(queryXML).getProperty("queryVENTAS_COMERCIO_COUNT");
        return query;
    }

    /*********************
     ***** QUERY MCP *****
     *********************/

    public static String queryEJECUCION_PROCESO_TX_MONETARIAS() {
        String query = fetchQuery(queryXML).getProperty("queryEJECUCION_PROCESO_TX_MONETARIAS");
        return query;
    }

    public static String queryEJECUCION_PROCESO_COUNT_TX_MONETARIAS() {
        String query = fetchQuery(queryXML).getProperty("queryEJECUCION_PROCESO_COUNT_TX_MONETARIAS");
        return query;
    }

    public static String queryEJECUCION_PROCESO_TX_MONETARIAS_HIST() {
        String query = fetchQuery(queryXML).getProperty("queryEJECUCION_PROCESO_TX_MONETARIAS_HIST");
        return query;
    }

    public static String queryEJECUCION_PROCESO_COUNT_TX_MONETARIAS_HIST() {
        String query = fetchQuery(queryXML).getProperty("queryEJECUCION_PROCESO_COUNT_TX_MONETARIAS_HIST");
        return query;
    }

    public static String queryEJECUCION_PROCESO_TX_PROCESADAS() {
        String query = fetchQuery(queryXML).getProperty("queryEJECUCION_PROCESO_TX_PROCESADAS");
        return query;
    }

    public static String queryEJECUCION_PROCESO_COUNT_TX_PROCESADAS() {
        String query = fetchQuery(queryXML).getProperty("queryEJECUCION_PROCESO_COUNT_TX_PROCESADAS");
        return query;
    }
}
