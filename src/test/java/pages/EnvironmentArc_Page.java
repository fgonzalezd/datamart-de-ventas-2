package pages;

public class EnvironmentArc_Page  extends Base_Page {

    public static String environment = "src/test/resources/query/Environment_Arc.xml";

    /*************************
     ***** CREATE ORIGEN *****
     *************************/

    public static String createTIPO_RUBRO() {
        String create = fetchQuery(environment).getProperty("createTIPO_RUBRO");
        return create;
    }

    public static String createACTIVIDAD_AMEX() {
        String create = fetchQuery(environment).getProperty("createACTIVIDAD_AMEX");
        return create;
    }

    public static String createACTIVIDAD_ECONOMICA() {
        String create = fetchQuery(environment).getProperty("createACTIVIDAD_ECONOMICA");
        return create;
    }

    public static String createCONTRATOS() {
        String create = fetchQuery(environment).getProperty("createCONTRATOS");
        return create;
    }

    public static String createESTABLECIMIENTOS() {
        String create = fetchQuery(environment).getProperty("createESTABLECIMIENTOS");
        return create;
    }

    public static String createDIRECCIONES() {
        String create = fetchQuery(environment).getProperty("createDIRECCIONES");
        return create;
    }

    public static String createTIPO_CONTRATO() {
        String create = fetchQuery(environment).getProperty("createTIPO_CONTRATO");
        return create;
    }

    public static String createEECC_SPA() {
        String create = fetchQuery(environment).getProperty("createEECC_SPA");
        return create;
    }

    public static String createTIPO_CONTACTO() {
        String create = fetchQuery(environment).getProperty("createTIPO_CONTACTO");
        return create;
    }

    /****************************
     ***** DROPTABLE ORIGEN *****
     ****************************/

    public static String dropTIPO_RUBRO() {
        String create = fetchQuery(environment).getProperty("dropTIPO_RUBRO");
        return create;
    }

    public static String dropACTIVIDAD_AMEX() {
        String create = fetchQuery(environment).getProperty("dropACTIVIDAD_AMEX");
        return create;
    }

    public static String dropACTIVIDAD_ECONOMICA() {
        String create = fetchQuery(environment).getProperty("dropACTIVIDAD_ECONOMICA");
        return create;
    }

    public static String dropCONTRATOS() {
        String create = fetchQuery(environment).getProperty("dropCONTRATOS");
        return create;
    }

    public static String dropESTABLECIMIENTOS() {
        String create = fetchQuery(environment).getProperty("dropESTABLECIMIENTOS");
        return create;
    }

    public static String dropDIRECCIONES() {
        String create = fetchQuery(environment).getProperty("dropDIRECCIONES");
        return create;
    }

    public static String dropTIPO_CONTRATO() {
        String create = fetchQuery(environment).getProperty("dropTIPO_CONTRATO");
        return create;
    }

    public static String dropEECC_SPA() {
        String create = fetchQuery(environment).getProperty("dropEECC_SPA");
        return create;
    }

    public static String dropTIPO_CONTACTO() {
        String create = fetchQuery(environment).getProperty("dropTIPO_CONTACTO");
        return create;
    }

    /**********************
     ***** DELETE RAW *****
     **********************/

    public static String deleteTIPO_RUBRO() {
        String create = fetchQuery(environment).getProperty("deleteTIPO_RUBRO");
        return create;
    }

    public static String deleteACTIVIDAD_AMEX() {
        String create = fetchQuery(environment).getProperty("deleteACTIVIDAD_AMEX");
        return create;
    }

    public static String deleteACTIVIDAD_ECONOMICA() {
        String create = fetchQuery(environment).getProperty("deleteACTIVIDAD_ECONOMICA");
        return create;
    }

    public static String deleteCONTRATOS() {
        String create = fetchQuery(environment).getProperty("deleteCONTRATOS");
        return create;
    }

    public static String deleteESTABLECIMIENTOS() {
        String create = fetchQuery(environment).getProperty("deleteESTABLECIMIENTOS");
        return create;
    }

    public static String deleteDIRECCIONES() {
        String create = fetchQuery(environment).getProperty("deleteDIRECCIONES");
        return create;
    }

    public static String deleteTIPO_CONTRATO() {
        String create = fetchQuery(environment).getProperty("deleteTIPO_CONTRATO");
        return create;
    }

    public static String deleteEECC_SPA() {
        String create = fetchQuery(environment).getProperty("deleteEECC_SPA");
        return create;
    }

    public static String deleteTIPO_CONTACTO() {
        String create = fetchQuery(environment).getProperty("deleteTIPO_CONTACTO");
        return create;
    }

    /**********************
     ***** DELETE MCP *****
     **********************/

    public static String deletePROCESO_METRICA_REL() {
        String create = fetchQuery(environment).getProperty("deletePROCESO_METRICA_REL");
        return create;
    }

    public static String deletePROCESO_METRICA_REL_DIRECCIONES() {
        String create = fetchQuery(environment).getProperty("deletePROCESO_METRICA_REL_DIRECCIONES");
        return create;
    }

    public static String deletePROCESO_METRICA_REL_TIPO_CONTRATO() {
        String create = fetchQuery(environment).getProperty("deletePROCESO_METRICA_REL_TIPO_CONTRATO");
        return create;
    }

    public static String deletePROCESO_METRICA_REL_EECC_SPA() {
        String create = fetchQuery(environment).getProperty("deletePROCESO_METRICA_REL_EECC_SPA");
        return create;
    }

    public static String deletePROCESO_METRICA_REL_TIPO_CONTACTO() {
        String create = fetchQuery(environment).getProperty("deletePROCESO_METRICA_REL_TIPO_CONTACTO");
        return create;
    }

    public static String deleteEJECUCION_PROCESO() {
        String create = fetchQuery(environment).getProperty("deleteEJECUCION_PROCESO");
        return create;
    }

    /**********************
     ***** DELETE LOG *****
     **********************/

    public static String logESTABLECIMIENTOS() {
        String create = fetchQuery(environment).getProperty("logESTABLECIMIENTOS");
        return create;
    }

    public static String logDIRECCIONES() {
        String create = fetchQuery(environment).getProperty("logDIRECCIONES");
        return create;
    }

    public static String logTIPO_CONTRATO() {
        String create = fetchQuery(environment).getProperty("logTIPO_CONTRATO");
        return create;
    }

    public static String logEECC_SPA() {
        String create = fetchQuery(environment).getProperty("logEECC_SPA");
        return create;
    }

    public static String logTIPO_CONTACTO() {
        String create = fetchQuery(environment).getProperty("logTIPO_CONTACTO");
        return create;
    }

    /***********************
     ***** DELETE HDFS *****
     ***********************/

    public static String hdfsESTABLECIMIENTOS() {
        String create = fetchQuery(environment).getProperty("hdfsESTABLECIMIENTOS");
        return create;
    }

    public static String hdfsDIRECCIONES() {
        String create = fetchQuery(environment).getProperty("hdfsDIRECCIONES");
        return create;
    }

    public static String hdfsTIPO_CONTRATO() {
        String create = fetchQuery(environment).getProperty("hdfsTIPO_CONTRATO");
        return create;
    }

    public static String hdfsEECC_SPA() {
        String create = fetchQuery(environment).getProperty("hdfsEECC_SPA");
        return create;
    }

    public static String hdfsTIPO_CONTACTO() {
        String create = fetchQuery(environment).getProperty("hdfsTIPO_CONTACTO");
        return create;
    }
}
